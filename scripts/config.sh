#!/bin/bash

declare -A fct_returns

function error_handling {
  local msg=$1; local err_code=$2;
  test $err_code -eq 0 &&
    echo -e "$msg : \033[32mSuccess\033[0m" ||
    echo -e "$msg : \033[31mError\033[0m"
}

function add_env_vars {
  echo '' >> $HOME/.profile || return 1
  echo 'export XDG_CONFIG_HOME="$HOME/.config"' >> $HOME/.profile
  echo 'export XDG_CACHE_HOME="$HOME/.cache"' >> $HOME/.profile
  echo 'export ZDOTDIR="$HOME/.config/zsh"' >> $HOME/.profile
  echo 'export ZSH="$HOME/.config/oh-my-zsh"' >> $HOME/.profile
  echo 'export HISTFILE="$MY_CONFDIR/zsh/.zsh_history"' >> $HOME/.profile
  echo '' >> $HOME/.profile
  echo 'export EDITOR=nvim' >> $HOME/.profile
}

function install_programs {
  PROGS='tmux git zsh vim neovim curl neofetch emacs python3 python3-pip'
  command -v apt &> /dev/null && sudo apt install $PROGS && return 0
  command -v pacman &> /dev/null && sudo pacman -Sy $PROGS && return 0
}

function install_zsh {
  mkdir -p $HOME/.config/zsh
  echo -e "n\n" | sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
  sudo usermod -s /usr/bin/zsh $USER

  # Installing zsh plugins
  git clone https://github.com/agkozak/zsh-z $ZSH_CUSTOM/plugins/zsh-z
  git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $ZSH_CUSTOM/plugins/zsh-syntax-highlighting
}

function install_vundle {
  git clone https://github.com/VundleVim/Vundle.vim.git $XDG_CONFIG_HOME/vim/bundle/Vundle.vim
}

function install_doom_emacs {
  read -p "Install Doom Emacs ? [y/n]" choice
  case "$choice" in
    y|Y )
      git clone --depth 1 https://github.com/hlissner/doom-emacs $HOME/.emacs.d &&
      $HOME/.emacs.d/bin/doom install && return 0 || return 1 ;;
    n|N ) return 0 ;;
    * ) return 0 ;;
  esac
}

function sync_doom_emacs {
  test -f $HOME/.emacs.d/bin/doom &&
    $HOME/.emacs/bin/doom sync && return 0 || return 1;
}

function cloning_config {
  DOTFILES=$HOME/.cfg
  BACKUP=$HOME/.cfg-backup
  REPO=https://gitlab.com/j.pilleux/dotfiles.git
  [ -d "$DOTFILES" ] && \
    echo -e "\033[31mERROR\033[0m: $DOTFILES folder already exists" && \
    return 1;

  git clone --bare $REPO $DOTFILES &&
  git --git-dir=$DOTFILES --work-tree=$HOME config status.showUntrackedFiles no &&
  mkdir -p $BACKUP &&
  git --git-dir=$DOTFILES --work-tree=$HOME checkout 2>&1 | \
  egrep "\s+\." | awk {'print $1'} | xargs -I{} bash -c 'mv -f {} $HOME/.cfg-backup/"$(basename {})"' &&
  git --git-dir=$DOTFILES --work-tree=$HOME checkout && return 0 || return 1
}

# Force the script to be executed from home
cd $HOME;

echo "Exportin env variables for current script."
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export ZDOTDIR="$HOME/.config/zsh"
export ZSH="$HOME/.config/oh-my-zsh"
export HISTFILE="$MY_CONFDIR/zsh/.zsh_history"
export ZSH_CUSTOM=$ZSH/custom

echo "Saving env variables."
add_env_vars &> /dev/null
fct_returns["Add env variables"]=$?

echo "Installing basic programs."
install_programs &> /dev/null
fct_returns["Programs installation"]=$?

echo "Installing ZSH shell and it's modules."
install_zsh &> /dev/null
fct_returns["ZSH installation"]=$?

echo "Installing vundle."
install_vundle &> /dev/null
fct_returns["Vundle installation"]=$?

echo "Installing Doom Emacs."
install_doom_emacs
fct_returns["Doom Emacs installation"]=$?

echo "Cloning configuration."
cloning_config &> /dev/null
fct_returns["Config cloning"]=$?

echo "Syncing Doom Emacs."
sync_doom_emacs 
fct_returns["Doom Emacs sync"]=$?

echo -e "\nSummary\n"
for elem in "${fct_returns[@]}"; do
  test ${fct_returns[$elem]} -eq 0 &&
    echo -e "$elem : \033[32mSuccess\033[0m" ||
    echo -e "$elem : \033[31mError\033[0m";
done

exit 0;
